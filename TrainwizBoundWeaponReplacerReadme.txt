Bound Weapon Replacer

////Description/////

This is a simple replacement for all vanilla bound weapons. I found that there was no replacer for anything like that, so I decided to do one myself.
The following weapons are replaced:
-Bound Spear
-Bound Battleaxe
-Bound Longsword
-Bound Bow
-Bound Mace
-Bound Dagger

Armor is not affected.

////Installation////
Like any other mod, extract these files anywhere you like, then copy over/merge them with your data folder. This does require an ESP, so there may be some incompatibilities with mods that rebalance bound weapons, though you should be safe if you load this last.

////Additional Info////

If you've got questions, comments, or whatever, send me an email at trainwiz@yahoo.com

Special thanks to Saint Jiub, for making the new textures.