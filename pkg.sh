#!/bin/sh
set -e

modname=bound-balance
file_name=$modname.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths ${file_name} \
    CHANGELOG.md \
    LICENSE \
    README.md \
    TrainwizBoundWeaponReplacerReadme.txt \
    icons \
    meshes \
    textures \
    $modname-testing.omwaddon \
    $modname.omwaddon \
    $modname.omwscripts \
    l10n \
    scripts \
    version.txt

sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
