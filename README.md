# Bound Balance

Balances bound items by adjusting them based on the player's conjuration skill level.

**Requires OpenMW 0.49 or newer!**

If you have [OAAB_Data](https://www.nexusmods.com/morrowind/mods/49042) or [Tamriel_Data](https://www.nexusmods.com/morrowind/mods/44537) installed, assets from them will be used automatically.

#### Features

* Replaces bound items with scaled versions based on the player's Conjuration skill
    * Works with any spell or scroll that has one or more bound item effect
* Optionally uses [OAAB_Data](https://www.nexusmods.com/morrowind/mods/49042) and/or [Tamriel Data](https://www.nexusmods.com/morrowind/mods/44537) assets if they are installed (on by default)
* Optionally use assets from Trainwiz's [Bound Weapon Replacer](https://www.nexusmods.com/morrowind/mods/41778)
* Optionally limit the power level of bound items (limit 100 by default)
* Optionally re-equip gear you had on before summoning a bound item or items (on by default)

#### How It Works

* Each frame, the player's active spells and their effects are checked
* The player's equipped gear is also checked and noted (if this feature is enabled)
* When the effect of a bound item is found, the mod will work to replace it:
    * Data is kept internally for each spell, effect, and item associated with them
    * Replacement item IDs are given for each item
    * The vanilla bound items are deleted
    * The new, replacement items are spawned and put into the player's inventory
    * They are then equipped on the player in the correct slot
* Items will expire within the normal duration of the given spell effect
* If the player tries to drop an item it will despawn
* If the player travels or otherwise passes time in large amounts any bound items will despawn appropriately
* If the related feature is enabled, any gear that was previously equipped in a slot that was taken up by a bound item will be re-equipped

#### How To Test This Mod

If you're trying to work with the code or just wanting to see it do what it says it should do, follow these steps to test out all of the features of this mod.

Please note that it's recommended to enable debug logging prior to doing this, that can be done via the script settings menu at: ESC >> Options >> Scripts >> Bound Balance

1. Use the OpenMW feature to start a test character and spawn into `Seyda Neen`
1. Press \` to bring down the console
1. Type `player->additem sc_lordmhasvengeance 1` and press enter
1. Press \` to hide the console
1. Open your magic menu and select `Scroll of Lord Mhas' Vengeance`
1. Close the menu and cast the spell

At this point several bound items will be summoned and replaced by this mod. You can let them expire as normal, drop them, travel, train, or rest to make them expire.

The above step can be repeated with any spell from the game that has a bound item effect.

##### Test Plugin

Also included with this mod is a testing plugin: `bound-balance-testing.omwaddon`

Load this plugin to get access to pre-made spells that are designed to test this mod:

```
player->addspell momw_bb_testspell1
player->addspell momw_bb_testspell2
player->addspell momw_bb_testspell3
```

They each summon several bound items onto the player in order to ensure that multiple items can be properly handled at once.

#### Credits

Concept, design, Lua script: **johnnyhostile**

Designers:

* **Gonzo**
* **Ronik**
* **Sophie**

**Special Thanks**:

* **Trainwiz** for making [Bound Weapon Replacer](https://www.nexusmods.com/morrowind/mods/41778)
* **Benjamin Winger** for making [DeltaPlugin](https://gitlab.com/bmwinger/delta-plugin/)
* **The Morrowind Modding Community and the OAAB_Data Team** for making [OAAB_Data](https://www.nexusmods.com/morrowind/mods/49042) (where item assets are from)
* **The Tamriel Rebuilt Team** for making [Tamriel Rebuilt](https://www.tamriel-rebuilt.org/)
* **The OpenMW team, including every contributor** for making OpenMW and OpenMW-CS
* **The Modding-OpenMW.com team** for being amazing
* **All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server** for their dilligent testing <3
* **Bethesda** for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Localization

EN: johnnyhostile

SV: Lysol

#### Web

[Project Home](https://modding-openmw.gitlab.io/bound-balance/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/bound-balance)

#### Installation

**OpenMW 0.49 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/bound-balance/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Gameplay\bound-balance

        # Linux
        /home/username/games/OpenMWMods/Gameplay/bound-balance

        # macOS
        /Users/username/games/OpenMWMods/Gameplay/bound-balance

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Gameplay\bound-balance"`)
1. Add `content=bound-balance.omwscripts` and `content=bound-balance.omwaddon` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Known Issues / Limitations

* You will have the vanilla version of a bound item for roughly one frame before this mod is able to do its thing
* The vanilla behavior of preventing dropping and selling of bound items can't be replicated exactly at this time
  * If you drop a bound item from this mod, it simply vanishes on the next frame
  * If you sell a bound item from this mod, they have no value and vanish as if you've dropped them

#### Compatibility

* This mod should be compatible with anything that doesn't alter the bound items (this one looks specifically for the vanilla items).

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/bound-balance/-/issues)
* Email `bound-balance@modding-openmw.com`
* Contact the author on Discord: `@johnnyhostile`
* Contact the author on Libera.chat IRC: `johnnyhostile`

#### Planned Features

* Fix known issues
* Anything else? Suggest a feature [on GitLab](https://gitlab.com/modding-openmw/bound-balance/-/issues)
